# django-nginx-docker
这是一个专门为python3，django2准备的项目，其中用到了uwsgi，多项目控制。

也用到了nginx，可以多端口，多域名控制。

**欢迎提供代码完善此项目。**



## 使用流程

1. 在django文件中，将pip需要安装的包，写入requirements文件。
2. 把supervisor的conf文件，放入django/supervisor文件夹。以.conf结尾。（supervisor —> /etc/supervisor/conf.d）
3. 将nginx的conf文件，放入nginx/nginx_conf文件夹中。必须以.conf结尾。
4. 在项目根目录，执行bash/run.sh文件。



## 注意

django项目中，包含了mysql，开发中没有使用到。如有需要，请自行调配。并提交代码。

