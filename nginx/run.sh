docker run \
    --link django2:django2 \
    --name nginx_server \
    -p 80:80 \
    --volumes-from django2 \
    -v $(pwd)/nginx_conf:/etc/nginx/conf.d \
    -itd glittering/nginx 
